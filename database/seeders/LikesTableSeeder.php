<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Database\Factories\LikeFactory;
use Domains\Posts\Models\Post;

class LikesTableSeeder extends Seeder
{
    public function run()
    {
        $posts = Post::all()->take(50);

        foreach ($posts as $post) {
            LikeFactory::new()->create([
                'post_id' => $post->id,
                'user_id' => User::factory(),
            ]);
        }

        Post::all()->each(function ($post) {
            $post->update(['count_likes' => $post->likes()->count()]);
        });
    }
}