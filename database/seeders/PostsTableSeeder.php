<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Factories\PostFactory;

class PostsTableSeeder extends Seeder
{
    public function run()
    {
        PostFactory::new()->count(100)->create();
    }
}
