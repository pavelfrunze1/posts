## Installation

1. Clone the repository

2. Create a `.env` file:

   copy from `.env.example` into `.env`

3. Create a database:
   - Create a new database named `posts`

4. Install dependencies:

   `composer install`

5. Generate the application key:

   `php artisan key:generate`

6. Run the database migrations and seed the database:

   `php artisan migrate`
   
   `php artisan db:seed --class=PostsTableSeeder`

7. Start the application:

   `php artisan run`

8. Access the application:

   Open your web browser and visit `http://localhost:8000` to access the application.
