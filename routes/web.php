<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Posts\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/posts');
});

Route::get('/posts', [PostController::class, 'index'])->name('posts-index');
Route::get('/posts/{post}', [PostController::class, 'view'])->name('posts-show');
Route::post('/posts/{post}/like', [PostController::class, 'like'])->name('posts-like');
Route::delete('/posts/{post}/unlike', [PostController::class, 'unlike'])->name('posts-unlike');

Auth::routes();

Route::get('/home', function () {
    return redirect('/posts');
})->name('home');
