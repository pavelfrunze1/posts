<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class run extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the queue worker and serve the application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->comment('Starting queue worker and application server...');

        $queueWorker = new Process(['php', 'artisan', 'queue:work']);
        $applicationServer = new Process(['php', 'artisan', 'serve']);

        $queueWorker->start();
        $applicationServer->start();

        while ($queueWorker->isRunning() || $applicationServer->isRunning()) {
            usleep(200000);
        }
    }
}
