<?php

namespace App\Http\Controllers\Posts;

use Domains\Likes\Actions\LikePostAction;
use Domains\Likes\Actions\UnlikePostAction;
use Domains\Posts\Models\Post;
use App\Http\Controllers\Controller;
use Domains\Likes\Repositories\LikeRepository;
use Domains\Likes\Services\LikeService;
use Domains\Posts\Repositories\PostRepository;
use Exception;

class PostController extends Controller
{
    public function index(PostRepository $postRepository, LikeRepository $likeRepository) {

        $userLikedPosts = auth()->check() ? $likeRepository->getLikedPostsByUserId(auth()->id()) : [];

        return view('posts.index', [
                'posts' => $postRepository->paginatePosts(10), 
                'userLikedPosts' => $userLikedPosts
            ]
        );
    }

    public function view(Post $post, LikeRepository $likeRepository)
    {
        $isLikedByUser = false;

        if (auth()->check()) {
            $isLikedByUser = $likeRepository->isLikedByUser($post->id, auth()->id());
        }

        return view('posts.view', [
            'post' => $post,
            'isLikedByUser' => $isLikedByUser,
        ]);
    }

    public function like(Post $post, LikePostAction $likePostAction)
    {
        try {
            $likePostAction->execute($post, auth()->user());
        } catch (Exception $e) {
            response()->json([['error' => $e->getMessage()]], 422);
        }

        response()->json([
            'ok' => true,
        ]);
    }

    public function unlike(Post $post, UnlikePostAction $unlikePostAction)
    {
        try {
            $unlikePostAction->execute($post, auth()->user());
        } catch (Exception $e) {
            response()->json([['error' => $e->getMessage()]], 422);
        }

        response()->json([
            'ok' => true,
        ]);
    }
}
