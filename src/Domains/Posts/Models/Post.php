<?php

namespace Domains\Posts\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Domains\Likes\Models\Like;

class Post extends Model
{
    use HasFactory;

	protected $fillable = [
		'title',
        'body',
        'count_likes'
    ];

	public function likes()
    {
        return $this->hasMany(Like::class);
    }
}
