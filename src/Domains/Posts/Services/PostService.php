<?php

namespace Domains\Posts\Services;

use Domains\Posts\Repositories\PostRepository;

class PostService
{
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function create($data)
    {
        return $this->postRepository->create($data);
    }

    public function find($id)
    {
        return $this->postRepository->find($id);
    }

    public function getAll()
    {
        return $this->postRepository->getAll();
    }
}
