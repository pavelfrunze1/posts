<?php

namespace Domains\Posts\Repositories;

use Domains\Posts\Models\Post;

class PostRepository
{
    public function create($data)
    {
        return Post::create($data);
    }

    public function find($id)
    {
        return Post::findOrFail($id);
    }

    public function getAll()
    {
        return Post::all();
    }

	public function paginatePosts(int $perPage): object
    {
        $posts = Post::paginate($perPage);
        return $posts;
    }
}
