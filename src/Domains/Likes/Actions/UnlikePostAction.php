<?php

namespace Domains\Likes\Actions;

use Domains\Likes\Models\Like;
use Domains\Likes\Services\LikeService;
use Domains\Posts\Models\Post;
use App\Models\User;
use Domains\Likes\Jobs\UnlikePostJob;
use Domains\Likes\Repositories\LikeRepository;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Support\Facades\Cache;

class UnlikePostAction
{
    protected $likeRepository;

    public function __construct(LikeRepository $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }

    public function execute(Post $post, User $user)
    {
        $cacheKey = "liked_posts_user_{$user->id}";

        $lock = Cache::lock($cacheKey);

        try {
            $likedPosts = $this->likeRepository->getLikedPostsByUserId($user->id);
            $likedPosts = array_diff($likedPosts, [$post->id]);
            Cache::put($cacheKey, $likedPosts, config('app.users_likes_cache_time'));

            dispatch(new UnlikePostJob($user, $post));
        } catch (LockTimeoutException $e) {

        } finally {
            $lock->release();
        }


        return true;
    }
}
