<?php

namespace Domains\Likes\Actions;

use Domains\Likes\Services\LikeService;
use Domains\Posts\Models\Post;
use App\Models\User;
use Domains\Likes\Jobs\LikePostJob;
use Domains\Likes\Repositories\LikeRepository;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Support\Facades\Cache;

class LikePostAction
{
    protected $likeRepository;

    public function __construct(LikeRepository $likeRepository)
    {
        $this->likeRepository = $likeRepository;
    }

    public function execute(Post $post, User $user): bool
    {
        $cacheKey = "liked_posts_user_{$user->id}";

        $lock = Cache::lock($cacheKey);

        try {
            $likedPosts = $this->likeRepository->getLikedPostsByUserId($user->id);
            $likedPosts[] = $post->id;
            Cache::put($cacheKey, $likedPosts, config('app.users_likes_cache_time'));

            dispatch(new LikePostJob($user, $post));
        } catch (LockTimeoutException $e) {

        } finally {
            $lock->release();
        }


        return true;
    }
}
