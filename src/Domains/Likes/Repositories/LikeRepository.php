<?php

namespace Domains\Likes\Repositories;

use Domains\Likes\Models\Like;
use Illuminate\Support\Facades\Cache;

class LikeRepository
{
    public function create($data)
    {
        return Like::create($data);
    }

    public function delete($id)
    {
        return Like::findOrFail($id)->delete();
    }

    public function findByUserAndPost($userId, $postId)
    {
        return Like::where('user_id', $userId)->where('post_id', $postId)->first();
    }

    public function getLikedPostsByUserId(int $userId): array
    {
        $cacheKey = "liked_posts_user_{$userId}";

        return Cache::remember($cacheKey, env('USERS_LIKES_CACHE_TIME'), function () use ($userId) {
            return Like::where('user_id', $userId)->pluck('post_id')->toArray();
        });
    }

    public function isLikedByUser(int $postId, int $userId): bool
    {
        $likedPosts = $this->getLikedPostsByUserId($userId);
        return in_array($postId, $likedPosts);
    }

    public function unlike($userId, $postId)
    {
        return Like::where('user_id', $userId)->where('post_id', $postId)->delete();

    }
}
