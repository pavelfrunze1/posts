<?php

namespace Domains\Likes\Jobs;

use Domains\Likes\Repositories\LikeRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UnlikePostJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $post;

    public function __construct($user, $post)
    {
        $this->user = $user;
        $this->post = $post;
    }

    public function handle(LikeRepository $likeRepository)
    {
        $likeRepository->unlike($this->user->id, $this->post->id);
        $this->post->decrement('count_likes');
    }
}
