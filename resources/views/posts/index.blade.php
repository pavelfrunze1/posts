@extends('layouts.app')
{{-- {{dd($userLikedPosts)}} --}}
@section('content')
    <div class="container">
		@foreach ($posts as $post)
			<div class="card mb-4">
				<div class="card-header">
					<a href="{{"/posts/$post->id"}}"><h3>{{ $post->title }}</h3></a>
				</div>
				<div class="card-body">
					<p>{{ $post->body }}</p>
					<div class="likes-section">
						@if (auth()->check())
							<button data-post-id="{{$post->id}}" class="btn likeUnlikePostBtn {{in_array($post->id, $userLikedPosts) ? "btn-danger" : "btn-primary"}}">
								{{in_array($post->id, $userLikedPosts) ? "Unlike" : "Like"}}
							</button>
							<span class="likes-label">Likes: </span>
							<span class="likes-count">{{$post->count_likes}}</span>
						@else
							<p><a href="{{ route('login') }}">Login</a> to like this post.</p>
						@endif
					</div>
				</div>
			</div>
		@endforeach

		<div class="d-flex justify-content-center">{{ $posts->links('pagination-custom') }}</div>
    </div>
    @push('js')
        <script>
            $(".likeUnlikePostBtn").click(likeUnlikePost)
            function likeUnlikePost(event) {
                let route = $(event.target).hasClass("btn-primary") ? 'like' : 'unlike';
                $.ajax({
                    url: `/posts/${$(event.target).data('post-id')}/${route}`,
                    method: $(event.target).hasClass("btn-primary") ? 'POST' : 'DELETE',
                    success: function() {
                        
                    },
                    error: function() {
                        updateNumberOfLikes()
                        changeIcon()
                    }
                });
                updateNumberOfLikes(event)
                changeIcon(event)
            }

            function changeIcon(event) {
                if ($(event.target).hasClass('btn-danger')) {
                    $(event.target).removeClass('btn-danger').addClass('btn-primary').text('Like');
                } else {
                    $(event.target).removeClass('btn-primary').addClass('btn-danger').text('Unlike');
                }
            }

            function updateNumberOfLikes(event) {
                if ($(event.target).hasClass("btn-primary")) {
                    $(event.target).siblings(".likes-count").eq(0).text(parseInt( $(event.target).siblings(".likes-count").eq(0).text()) + 1)
                } else {
                    if (parseInt($(".likes-count").eq(0).text()) != 0) {
                        $(event.target).siblings(".likes-count").eq(0).text(parseInt( $(event.target).siblings(".likes-count").eq(0).text()) - 1)
                    }
                }
            }
        </script>
    @endpush
    @push('css')
        <style>
        .likes-label {
                font-weight: bold;
                margin-left: 10px;
                font-size: 1.1em;
            }
            .likes-section {
                display: flex;
                align-items: center;
            }
            .likes-count {
                display: inline-block;
                padding: 4px 8px;
                background-color: #f1f1f1;
                color: #333;
                font-weight: bold;
                border-radius: 4px;
                margin-left: 5px;
            }
        </style>
    @endpush
@endsection