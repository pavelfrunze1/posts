@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>{{ $post->title }}</h3>
            </div>
            <div class="card-body">
                <p>{{ $post->body }}</p>
                <div class="likes-section">
                    @if (auth()->check())
                        <button id="likeUnlikePostBtn" class="btn {{$isLikedByUser ? "btn-danger" : "btn-primary"}}">
                            {{$isLikedByUser ? "Unlike" : "Like"}}
                        </button>
                        <span class="likes-label">Likes: </span>
                        <span class="likes-count">{{$post->count_likes}}</span>
                    @else
                        <p><a href="{{ route('login') }}">Login</a> to like this post.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @push('js')
        <script>
            $("#likeUnlikePostBtn").click(likeUnlikePost)
            function likeUnlikePost() {
                let route = $("#likeUnlikePostBtn").hasClass("btn-primary") ? "{{route('posts-like', $post->id)}}" : "{{route('posts-unlike', $post->id)}}";
                $.ajax({
                    url: route,
                    method: $("#likeUnlikePostBtn").hasClass("btn-primary") ? 'POST' : 'DELETE',
                    success: function() {
                        
                    },
                    error: function() {
                        updateNumberOfLikes()
                        changeIcon()
                    }
                });
                updateNumberOfLikes()
                changeIcon()
            }

            function changeIcon() {
                if ($('#likeUnlikePostBtn').hasClass('btn-danger')) {
                    $('#likeUnlikePostBtn').removeClass('btn-danger').addClass('btn-primary').text('Like');
                } else {
                    $('#likeUnlikePostBtn').removeClass('btn-primary').addClass('btn-danger').text('Unlike');
                }
            }

            function updateNumberOfLikes() {
                if ($("#likeUnlikePostBtn").hasClass("btn-primary")) {
                    $(".likes-count").eq(0).text(parseInt($(".likes-count").eq(0).text()) + 1)
                } else {
                    if (parseInt($(".likes-count").eq(0).text()) != 0) {
                        $(".likes-count").eq(0).text(parseInt($(".likes-count").eq(0).text()) - 1)
                    }
                }
            }
        </script>
    @endpush
    @push('css')
        <style>
        .likes-label {
                font-weight: bold;
                margin-left: 10px;
                font-size: 1.1em;
            }
            .likes-section {
                display: flex;
                align-items: center;
            }
            .likes-count {
                display: inline-block;
                padding: 4px 8px;
                background-color: #f1f1f1;
                color: #333;
                font-weight: bold;
                border-radius: 4px;
                margin-left: 5px;
            }
        </style>
    @endpush
@endsection