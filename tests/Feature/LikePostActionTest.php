<?php

use Domains\Likes\Actions\LikePostAction;
use App\Models\User;
use Domains\Likes\Jobs\LikePostJob;
use Domains\Likes\Repositories\LikeRepository;
use Domains\Posts\Models\Post;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cache;

class LikePostActionTest extends Tests\TestCase
{
    public function testExecute()
    {
        $likeRepository = Mockery::mock(LikeRepository::class);

        $user = new User();
        $user->id = 1;

        $post = new Post();
        $post->id = 1;

        $likedPosts = [2, 3];
        $expectedLikedPosts = [2, 3, 1];

        $cacheKey = "liked_posts_user_{$user->id}";

        $lock = Mockery::mock();
        $lock->shouldReceive('release')->once();

        $cache = Mockery::mock();
        $cache->shouldReceive('lock')->andReturn($lock);
        $cache->shouldReceive('put')
            ->with($cacheKey, $expectedLikedPosts, Mockery::any())
            ->once();

        Cache::swap($cache);

        $likeRepository->shouldReceive('getLikedPostsByUserId')
            ->with($user->id)
            ->once()
            ->andReturn($likedPosts);

        Bus::fake([LikePostJob::class]);

        dispatch(new LikePostJob($user->id, $post->id));

        Bus::assertDispatched(LikePostJob::class, function () {
            return true;
        });

        $likePostAction = new LikePostAction($likeRepository);
        $result = $likePostAction->execute($post, $user);

        $this->assertTrue($result);
    }
}
