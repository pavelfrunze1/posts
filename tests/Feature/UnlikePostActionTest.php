<?php

use Domains\Likes\Actions\UnlikePostAction;
use App\Models\User;
use Domains\Likes\Jobs\UnlikePostJob;
use Domains\Likes\Repositories\LikeRepository;
use Domains\Posts\Models\Post;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cache;

class UnlikePostActionTest extends Tests\TestCase
{
    public function testExecute()
    {
        $likeRepository = Mockery::mock(LikeRepository::class);

        $user = new User();
        $user->id = 1;

        $post = new Post();
        $post->id = 1;

        $likedPosts = [2, 3, 1];
        $expectedLikedPosts = [2, 3];

        $cacheKey = "liked_posts_user_{$user->id}";

		$lock = Mockery::mock();
        $lock->shouldReceive('release')->once();

        $cache = Mockery::mock();
        $cache->shouldReceive('lock')->andReturn($lock);
        $cache->shouldReceive('put')
            ->with($cacheKey, $expectedLikedPosts, Mockery::any())
            ->once();

        Cache::swap($cache);

        $likeRepository->shouldReceive('getLikedPostsByUserId')
            ->with($user->id)
            ->once()
            ->andReturn($likedPosts);

        Bus::fake([UnlikePostJob::class]);

        dispatch(new UnlikePostJob($user->id, $post->id));

        Bus::assertDispatched(UnlikePostJob::class, function () {
            return true;
        });

        $unlikePostAction = new UnlikePostAction($likeRepository);
        $result = $unlikePostAction->execute($post, $user);

        $this->assertTrue($result);
    }
}
